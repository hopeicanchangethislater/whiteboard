#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include <QBrush>
#include <QPen>
#include <QPixmap>
#include <QWidget>
#include <QMouseEvent>
#include <vector>
#include <map>
#include <mutex>

#include "serialiser.h"

namespace Ui {
class Window;
}

class Window : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit Window(QWidget *parent = 0);
    ~Window();
    std::vector < std::vector <int> > xLines;
    std::vector < std::vector <int> > yLines;
    std::vector <int> x;
    std::vector <int> y;
    std::vector <int> currentX;
    std::vector <int> currentY;
    std::vector <QColor> lineColours;
    std::vector <int> lineWidth;
    std::vector <int> recvx;
    std::vector <int> recvy;
    std::vector <QColor> recvColor;
    std::vector <int> recvW;
    std::vector < std::vector <int> > recvxLines;
    std::vector < std::vector <int> > recvyLines;
    std::vector <int> currentrecvX;
    std::vector <int> currentrecvY;

    void clear();
    int myWidth, myRed, myGreen, myBlue, myAlpha;

public slots:
    void on_eraseButton_clicked();
    
private slots:
    void on_comboBox_currentIndexChanged(const QString &arg1);
    void on_redSlider_valueChanged(int value);
    void on_greenSlider_valueChanged(int value);
    void on_blueSlider_valueChanged(int value);
    void on_AlphaSlider_valueChanged(int value);

private:
    Ui::Window *ui;
    QPalette* palette;
    QPen pen;
    QFont font;
    static void *recvDrawLoopInit(void* instance);
    void recvDrawLoop();

    int oldHeight, oldWidth;
    Serialiser serialiser;
    int oldX;
    int oldY;
    int dx, dy, dd;

    std::mutex pixelDataMutex;



protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
};

#endif // WINDOW_H
