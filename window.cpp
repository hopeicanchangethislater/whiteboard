#include "window.h"
#include "ui_window.h"
#include <QPainter>
#include <map>
#include <vector>
#include <QDebug>
#include <QWidget>
#include <iostream>
#include <stdlib.h>
#include "serialiser.h"
#include <math.h>

using namespace std;

Window::Window(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Window)
{

    serialiser.init(25, 1024, 1024, true);

    myWidth = 4;
    myRed = 0;
    myGreen = 0;
    myBlue = 0;
    myAlpha = 255;
    palette = new QPalette;
    font.setPixelSize(30);
    pen.setCapStyle(Qt::RoundCap);

    oldX = 9999999;
    oldY = 9999999;

    ui->setupUi(this);

    pthread_t recvPainter;
    pthread_create(&recvPainter, nullptr, Window::recvDrawLoopInit, (void*) this);

    oldWidth = width();
    oldHeight = height();
}

void* Window::recvDrawLoopInit(void *instance)
{
  Window *parent = (Window*) instance;
  parent->recvDrawLoop();
  return nullptr;
}

void Window::recvDrawLoop()
{
  Serialiser_t pixelData;
  while (1)
  {
    if (serialiser.dataAvailable())
    {
      cout << "Data available" << endl;
      pixelData = serialiser.getPixel();
      pixelDataMutex.lock();
      if (pixelData.getPoint().x() == 50000)
      {
        recvx = vector<int>();
        recvy = vector<int>();
      }
      else if (pixelData.getPoint().x() == 50001)
      {
        xLines.clear();
        yLines.clear();
        lineColours.clear();
        lineWidth.clear();
        recvx.clear();
        recvy.clear();
        recvxLines.clear();
        recvyLines.clear();
        recvColor.clear();
        recvW.clear();
      }
      else
      {
        recvx.push_back(pixelData.getPoint().x() + (width() / 2));
        recvy.push_back(pixelData.getPoint().y());
        recvxLines.push_back(recvx);
        recvyLines.push_back(recvy);
        recvColor.push_back(pixelData.getColour());
        recvW.push_back(pixelData.getWidth());
      }
      pixelDataMutex.unlock();
      update();
    }
  }
}

Window::~Window()
{
    delete ui;
}

void Window::mouseMoveEvent(QMouseEvent * event)
{
    // get click position
//    qDebug() << "Mouse x " << event->x() << " Mouse y " << event->y();
    if( event->x() < (width()/2) && event->y() < height() && event->x() > 0 && event->y() > 0)
    {
        x.push_back(event->x());
        y.push_back(event->y());
        update();

        // Gotta send fast
        dx = oldX - event->x();
        dy = oldY - event->y();
        dd = sqrt( pow(dx, 2) + pow(dy, 2) );

        if (dd > 10)
        {
          serialiser.serialise(event->x(), event->y(), myRed, myGreen, myBlue, myAlpha, myWidth);
          oldX = event->x();
          oldY = event->y();
        }

    }
}

void Window::mousePressEvent(QMouseEvent *)
{
    //x.push_back(0);
    //y.push_back(0);

    // Sending only colour information first
    serialiser.serialise(9999998, 9999998, myRed, myGreen, myBlue, myAlpha, myWidth);

    update();
}

void Window::mouseReleaseEvent(QMouseEvent *)
{
    xLines.push_back(x);
    yLines.push_back(y);
    lineColours.push_back(QColor(myRed, myGreen, myBlue, myAlpha));
    lineWidth.push_back(myWidth);
    x = vector<int>();
    y = vector<int>();
    serialiser.serialise(50000, 0, 0, 0, 0, 0, 0);
}

void Window::paintEvent(QPaintEvent *)
{
    cout << 1 << endl;
    palette->setColor(QPalette::Window, QColor(myRed, myGreen, myBlue, myAlpha));
    cout << 12 << endl;
    ui->colourBox->setPalette(*palette);
//    QPainter painter = _painter;
    cout << 13 << endl;
    QPainter painter(this);
    cout << 14 << endl;
    if ((oldWidth != width()) | (oldHeight != height()))
    {
      cout << 15 << endl;
      QPoint p5((width() / 2), 0), p6((width() / 2), height());
      cout << 16 << endl;
      painter.drawLine(p5, p6);
      cout << 17 << endl;
    }

    pixelDataMutex.lock();
    cout << 18 << endl;
    for (unsigned int i = 0; i < xLines.size(); i++)
    {
        pen.setColor(lineColours[i]);
        pen.setWidth(lineWidth[i]);
        painter.setPen(pen);
        currentX = xLines[i];
        currentY = yLines[i];
        for (unsigned int j = 1; j < currentX.size(); j++)
        {
            QPoint p3(currentX[j], currentY[j]), p4(currentX[j-1], currentY[j-1]);
            painter.drawLine(p3, p4);
        }
    }
    cout << 19 << endl;
    for (unsigned int k = 0; k < recvxLines.size(); k++)
    {
        pen.setColor(recvColor[k]);
        pen.setWidth(recvW[k]);
        painter.setPen(pen);
        currentrecvX = recvxLines[k];
        currentrecvY = recvyLines[k];
        for (unsigned int m = 1; m < currentrecvX.size(); m++)
        {
            QPoint p7(currentrecvX[m], currentrecvY[m]), p8(currentrecvX[m-1], currentrecvY[m-1]);
            painter.drawLine(p7, p8);
        }
    }
    cout << 110 << endl;
    pen.setWidth(myWidth);
    cout << 111 << endl;
    pen.setColor(QColor(myRed, myGreen, myBlue, myAlpha));
    cout << 112 << endl;
    painter.setPen(pen);
    cout << 112 << endl;
    //painter.setRenderHint(QPainter::Antialiasing);
    //painter.setRenderHint(QPainter::HighQualityAntialiasing);
//    painter.setFont(font);
    for (unsigned int counter = 1; counter < x.size(); counter++) //Loop given to me by Tom Zhao
        {
            QPoint p1(x[counter], y[counter]), p2(x[counter - 1], y[counter - 1]);
            painter.drawLine(p1, p2);
        }
    cout << 114 << endl;
    for (unsigned int counter = 1; counter < recvx.size(); counter++) //Loop given to me by Tom Zhao
        {
            QPoint p9(recvx[counter], recvy[counter]), p10(recvx[counter - 1], recvy[counter - 1]);
            painter.drawLine(p9, p10);
        }
    cout << 115 << endl;
    pixelDataMutex.unlock();
    cout << 115 << endl;
}

void Window::on_eraseButton_clicked()
{
    cout<<"clicked"<<endl;
    qDebug() << "Clicked";
    xLines.clear();
    yLines.clear();
    lineColours.clear();
    lineWidth.clear();
    serialiser.serialise(50001, 0, 0, 0, 0, 0, 0);
    update();
}

void Window::on_comboBox_currentIndexChanged(const QString &)
{
    myWidth = ui->comboBox->currentText().toInt();
    qDebug() << "Set Pen Width to: " << myWidth;
}

void Window::on_redSlider_valueChanged(int value)
{
    myRed = value;
    update();
}

void Window::on_greenSlider_valueChanged(int value)
{
    myGreen = value;
    update();
}

void Window::on_blueSlider_valueChanged(int value)
{
    myBlue = value;
    update();
}

void Window::on_AlphaSlider_valueChanged(int value)
{
    myAlpha = value;
    update();
}
