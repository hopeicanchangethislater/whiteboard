#ifndef SERIALISER_H
#define SERIALISER_H

#include <pthread.h>
#include <mutex>
#include <queue>
#include <QPoint>
#include <QColor>

#include "../PPC/ppc.h"

class Serialiser_t
{
  public:
    Serialiser_t(QPoint point, QColor colour, int width);
    Serialiser_t();
    QPoint getPoint();
    QColor getColour();
    int getWidth();

  private:
    QPoint _point;
    QColor _colour;
    int _width;
};

class Serialiser
{
public:
    void serialise(int x, int y, int r, int g, int b, int a, int width);
    void init(long unsigned int clockSpeed, unsigned int sendBufferSize, unsigned int recvBufferSize, bool checksum);
    bool dataAvailable();
    Serialiser_t getPixel();

private:
    static void *sendLoopInit(void* instance);
    void sendLoop();
    static void *recvLoopInit(void* instance);
    void recvLoop();

    std::mutex stringToSendMutex;
    std::mutex sendBufferMutex;
    std::mutex recvMutex;
    std::mutex readyToSendMutex;

    std::queue<char> sendBuffer;
    std::queue<int> recvX, recvY, recvR, recvG, recvB, recvA, recvWidth;

    char* stringToSend;
    int recvSize;
    int getsCalled;

    PPC_Connection *socketptr;

    pthread_t threads[2];
};

#endif // SERIALISER_H
