#include "serialiser.h"
#include "../PPC/ppc.h"
#include <stdio.h>

void Serialiser::init(unsigned long clockSpeed, unsigned int sendBufferSize, unsigned int recvBufferSize, bool checksum)
{
  recvSize = 0;
  getsCalled = 0;

  socketptr = new PPC_Connection(clockSpeed, sendBufferSize, recvBufferSize, checksum);
  socketptr->init();

//  stringToSend = _stringToSend;

  // Create separate threads for sending and receiving data
  pthread_create(&threads[0], nullptr, Serialiser::sendLoopInit, (void*) this);
  pthread_create(&threads[1], nullptr, Serialiser::recvLoopInit, (void*) this);
}

void Serialiser::serialise(int x, int y, int r, int g, int b, int a, int width)
{
  if (x == 9999998) // Colour information
  {
    sendBuffer.push('c');
    // These are all expected to be within 8 bits
    sendBuffer.push(r);
    sendBuffer.push(g);
    sendBuffer.push(b);
    sendBuffer.push(a);
    sendBuffer.push(width);
  }
  else if (x == 50001) // Colour information
  {
    sendBuffer.push('x');
  }
  else
  {
    // sendBufferMutex.lock();
    sendBuffer.push('s');

    // x and y coordinates are split into a high byte and a low byte
    sendBuffer.push((x >> 8) & 0xFF);
    sendBuffer.push(x & 0xFF);
    sendBuffer.push((y >> 8) & 0xFF);
    sendBuffer.push(y & 0xFF);

    sendBuffer.push('e');
    // sendBufferMutex.unlock();
  }
}

bool Serialiser::dataAvailable()
{
  return (recvSize > 0);
}

Serialiser_t Serialiser::getPixel()
{
  if (recvSize > 0)
  {
    recvSize--;
    int x, y, r, g, b, a, w;
    x = recvX.front();
    y = recvY.front();
    r = recvR.front();
    g = recvG.front();
    b = recvB.front();
    a = recvA.front();
    w = recvWidth.front();
    recvX.pop();
    recvY.pop();
    recvR.pop();
    recvG.pop();
    recvB.pop();
    recvA.pop();
    recvWidth.pop();
    return Serialiser_t(QPoint(x, y), QColor(r, g, b, a), w);
  }
  else
  {
    throw(PPC_Exception("No pixel data available."));
  }
}

void* Serialiser::sendLoopInit(void* instance)
{
  Serialiser *parent = (Serialiser*) instance;
  parent->sendLoop();
  return nullptr;
}

void Serialiser::sendLoop()
{
  while (1)
  {
    if (!sendBuffer.empty())
    {
      // If addToSendBuffer returns 0, there wasn't any space to add the data
      // Keep trying until there is space
      while (socketptr->addToSendBuffer(sendBuffer.front()) == 0);
      sendBuffer.pop();
    }
  }
}

void* Serialiser::recvLoopInit(void* instance)
{
  Serialiser *parent = (Serialiser*) instance;
  parent->recvLoop();
  return nullptr;
}

void Serialiser::recvLoop()
{
  int dataType = 0;
  int recvData;
  int highByte, x, y, r, g, b, a, width;
  while (1)
  {
    if (socketptr->recvBufferPopulated())
    {
      recvData = socketptr->getRecvBuffer();
      // Data should come in the same order as it was sent in
      // Refer to Serialiser::serialise()
      switch (dataType)
      {
        case 0:
        {
          if (recvData == 's')
          {
            fprintf(stdout, "Got start byte!\n");
            dataType++;
          }
          else if (recvData == 'c')
          {
            fprintf(stdout, "Got colour byte!\n");
            dataType = 5;
          }
          else if (recvData == 'x')
          {
            recvX.push(50001);
            recvY.push(0);
            recvR.push(0);
            recvG.push(0);
            recvB.push(0);
            recvA.push(0);
            recvWidth.push(0);
            recvSize++;
          }
          break;
        }
        case 1:
        {
          highByte = recvData;
          fprintf(stdout, "High x = %d\n", highByte);
          dataType++;
          break;
        }
        case 2:
        {
          fprintf(stdout, "Low x = %d\n", recvData);
          x = (highByte << 8) | recvData;
          fprintf(stdout, "x = %d\n", x);
          dataType++;
          break;
        }
        case 3:
        {
          highByte = recvData;
          fprintf(stdout, "High y = %d\n", highByte);
          dataType++;
          break;
        }
        case 4:
        {
          fprintf(stdout, "Low y = %d\n", recvData);
          y = (highByte << 8) | recvData;
          fprintf(stdout, "y = %d\n", y);
          dataType = 10;
          break;
        }
        case 5:
        {
          r = recvData;
          fprintf(stdout, "r = %d\n", r);
          dataType++;
          break;
        }
        case 6:
        {
          g = recvData;
          fprintf(stdout, "g = %d\n", g);
          dataType++;
          break;
        }
        case 7:
        {
          b = recvData;
          fprintf(stdout, "b = %d\n", b);
          dataType++;
          break;
        }
        case 8:
        {
          a = recvData;
          fprintf(stdout, "a = %d\n", a);
          dataType++;
          break;
        }
        case 9:
        {
          width = recvData;
          fprintf(stdout, "w = %d\n", width);
          dataType = 0;
          break;
        }
        case 10:
        {
          if (recvData == 'e')
          {
            fprintf(stdout, "Got pixel end byte!\n");
            recvX.push(x);
            recvY.push(y);
            recvR.push(r);
            recvG.push(g);
            recvB.push(b);
            recvA.push(a);
            recvWidth.push(width);
            recvSize++;
          }
          dataType = 0;
          break;
        }
      }
    }
  }
}

Serialiser_t::Serialiser_t(QPoint point, QColor colour, int width)
{
  _point = point;
  _colour = colour;
  _width = width;
}

QPoint Serialiser_t::getPoint()
{
  return _point;
}

QColor Serialiser_t::getColour()
{
  return _colour;
}

int Serialiser_t::getWidth()
{
  return _width;
}

Serialiser_t::Serialiser_t()
{
  _point = QPoint(0, 0);
  _colour = QColor(0, 0, 0, 0);
  _width = 0;
}
